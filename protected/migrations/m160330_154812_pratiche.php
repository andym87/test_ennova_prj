<?php

class m160330_154812_pratiche extends CDbMigration
{
	public function up()
	{

		$this->createTable('pratiche', array(
			'id' => 'pk NOT NULL',
			'id_pratica' => 'string NOT NULL',
			'data_creazione' => 'datetime',
			'stato_pratica' => 'enum(\'open\',\'close\') NOT NULL',
			'note' => 'text',
			'id_cliente' => 'int NOT NULL',
		));
	}

	public function down()
	{
		
		$this->dropTable('pratiche');
		echo "m160330_154812_pratiche does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
