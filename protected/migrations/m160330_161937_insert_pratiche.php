<?php

class m160330_161937_insert_pratiche extends CDbMigration
{
	public function up()
	{
		$this->insert('pratiche', array(
			'id_pratica'=>'0001',
			'data_creazione'=>'19771021',
			'stato_pratica'=>'open',
			'note'=>'test1',
			'id_cliente'=>'1',
		));
		$this->insert('pratiche', array(
			'id_pratica'=>'0010',
			'data_creazione'=>'19800912',
			'stato_pratica'=>'close',
			'note'=>'test2',
			'id_cliente'=>'2',
		));
		$this->insert('pratiche', array(
			'id_pratica'=>'0011',
			'data_creazione'=>'19831021',
			'stato_pratica'=>'open',
			'note'=>'test3',
			'id_cliente'=>'1',
		));
		$this->insert('pratiche', array(
			'id_pratica'=>'0100',
			'data_creazione'=>'19990917',
			'stato_pratica'=>'close',
			'note'=>'test4',
			'id_cliente'=>'3',
		));
		$this->insert('pratiche', array(
			'id_pratica'=>'0101',
			'data_creazione'=>'20020516',
			'stato_pratica'=>'open',
			'note'=>'test5',
			'id_cliente'=>'3',
		));
		$this->insert('pratiche', array(
			'id_pratica'=>'0111',
			'data_creazione'=>'20050519',
			'stato_pratica'=>'close',
			'note'=>'test6',
			'id_cliente'=>'4',
		));
	}

	public function down()
	{
		$this->truncateTable('pratiche');
		echo "m160330_161937_insert_pratiche does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
