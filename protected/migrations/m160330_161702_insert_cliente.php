<?php

class m160330_161702_insert_cliente extends CDbMigration
{
	public function up()
	{
		$this->insert('cliente', array(
			'nome'=>'nome1',
			'cognome'=>'cognome1',
			'codice_fiscale'=>'CGNNMO11X11X111X',
			'note'=>'prova1',
		));
		$this->insert('cliente', array(
			'nome'=>'nome2',
			'cognome'=>'cognome2',
			'codice_fiscale'=>'CGNNMO22X22X222X',
			'note'=>'prova2',
		));
		$this->insert('cliente', array(
			'nome'=>'nome3',
			'cognome'=>'cognome3',
			'codice_fiscale'=>'CGNNMO33X33X333X',
			'note'=>'prova3',
		));
		$this->insert('cliente', array(
			'nome'=>'nome4',
			'cognome'=>'cognome4',
			'codice_fiscale'=>'CGNNMO44X44X444X',
			'note'=>'prova4',
		));
		$this->insert('cliente', array(
			'nome'=>'nome5',
			'cognome'=>'cognome5',
			'codice_fiscale'=>'CGNNMO55X55X555X',
			'note'=>'prova5',
		));
		$this->insert('cliente', array(
			'nome'=>'nome6',
			'cognome'=>'cognome6',
			'codice_fiscale'=>'CGNNMO66X66X666X',
			'note'=>'prova6',
		));
	}

	public function down()
	{
		$this->dropForeignKey('cliente_pratica','pratiche');
		$this->truncateTable('cliente');		
		echo "m160330_161702_insert_cliente does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
