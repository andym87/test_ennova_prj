<?php

class m160330_155001_cliente extends CDbMigration
{
	public function up()
	{
		$this->createTable('cliente', array(
			'id' => 'pk',
			'nome' => 'string NOT NULL',
			'cognome' => 'string NOT NULL',
			'codice_fiscale' => 'string NOT NULL',
			'note' => 'text',
		));
			
		$this->addForeignKey("cliente_pratica","pratiche","id_cliente","cliente","id","CASCADE","CASCADE");		
		
	}

	public function down()
	{
		$this->dropTable('cliente');
		echo "m160330_155001_cliente does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
