<?php
/* @var $this PraticheController */
/* @var $model Pratiche */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); 
//$cliente = new Cliente();
?>
	<div class="row">
		<?php echo $form->label($model,'id_pratica'); ?>
		<?php echo $form->textField($model,'id_pratica',array('size'=>60,'maxlength'=>255)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'codice_fiscale'); ?>
		<?php echo $form->textField($model,'codice_fiscale',array('size'=>60,'maxlength'=>255)); ?>
	</div>
<!--
	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_creazione'); ?>
		<?php echo $form->textField($model,'data_creazione'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stato_pratica'); ?>
		<?php echo $form->textField($model,'stato_pratica',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'note'); ?>
		<?php echo $form->textArea($model,'note',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_cliente'); ?>
		<?php echo $form->textField($model,'id_cliente'); ?>
	</div>
-->
	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
