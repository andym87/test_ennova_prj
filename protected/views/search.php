<?php
/* @var $this PraticheController */
/* @var $model Pratiche */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pratiche-search-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_pratica'); ?>
		<?php echo $form->textField($model,'id_pratica'); ?>
		<?php echo $form->error($model,'id_pratica'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_creazione'); ?>
		<?php echo $form->textField($model,'data_creazione'); ?>
		<?php echo $form->error($model,'data_creazione'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stato_pratica'); ?>
		<?php echo $form->textField($model,'stato_pratica'); ?>
		<?php echo $form->error($model,'stato_pratica'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'note'); ?>
		<?php echo $form->textField($model,'note'); ?>
		<?php echo $form->error($model,'note'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_client'); ?>
		<?php echo $form->textField($model,'id_client'); ?>
		<?php echo $form->error($model,'id_client'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->