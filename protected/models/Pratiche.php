<?php

/**
 * This is the model class for table "pratiche".
 *
 * The followings are the available columns in table 'pratiche':
 * @property integer $id
 * @property string $id_pratica
 * @property string $data_creazione
 * @property string $stato_pratica
 * @property string $note
 * @property integer $id_cliente
 *
 * The followings are the available model relations:
 * @property Cliente $idCliente
 */
class Pratiche extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pratiche';
	}
	public $nome;
	public $codice_fiscale;
	public $cognome;
	//public $id_pratica;
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pratica, stato_pratica, id_cliente', 'required'),
			array('id_cliente', 'numerical', 'integerOnly'=>true),
			array('id_pratica', 'length', 'max'=>255),
			array('stato_pratica', 'length', 'max'=>5),
			array('data_creazione, note', 'safe'),
			array('nome, cognome, codice_fiscale', 'length', 'max'=>255),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('codice_fiscale, nome, id_pratica, data_creazione, stato_pratica, cognome, id_cliente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCliente' => array(self::BELONGS_TO, 'Cliente', 'id_cliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_pratica' => 'Id Pratica',
			'data_creazione' => 'Data Creazione',
			'stato_pratica' => 'Stato Pratica',
			'note' => 'Note',
			'id_cliente' => 'Id Cliente',
			'codice_fiscale' => 'Codice Fiscale',
			'nome'=>'Nome',
			'cognome'=>'Cognome',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('id',$this->id);
		//$criteria->compare('id_pratica',$this->id_pratica,true);
		//$criteria->compare('data_creazione',$this->data_creazione,true);
		//$criteria->compare('stato_pratica',$this->stato_pratica,true);
		//$criteria->compare('note',$this->note,true);
		//$criteria->compare('id_cliente',$this->id_cliente);
		//$criteria->with=array('idCliente');
		$criteria->alias ='pratiche';
		$criteria->select = 'pratiche.id_pratica, pratiche.data_creazione, pratiche.stato_pratica, cliente.codice_fiscale, cliente.nome, cliente.cognome';
		$criteria->join='LEFT JOIN cliente ON cliente.id=pratiche.id_cliente';
		$criteria->compare('id_pratica',$this->id_pratica,true);
		$criteria->compare('codice_fiscale',$this->codice_fiscale,true);
		//$criteria->order='id_cliente DESC';
		//$criteria->compare('codice_fiscale','cliente'->id);
		//$criteria->condition='Pratiche.codice_fiscale='.Yii::app()->Pratiche->codice_fiscale;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pratiche the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
